"""
    Graph Neural Network

    :author: Afan Secic (main developer), Anna Saranti (corrections and refactoring)
    :copyright: © 2020 HCI-KDD (ex-AI) group
    :date: 2020-12-01
"""

from torch_geometric.utils import get_laplacian
import torch
import torch.nn.functional as F
import torch.nn as nn
from torch_geometric.nn import GCNConv, global_add_pool, GINConv

from torch.nn import Sequential, Linear, ReLU, BatchNorm1d
from torch_geometric.nn.glob.glob import global_max_pool, global_mean_pool

class MUTAG_Classifier(torch.nn.Module):
    
    def __init__(self, input_dim, num_classes):
        super(MUTAG_Classifier, self).__init__()
        #Architecture taken from example for MUTAG dataset on graph classification
        dim = 32

        self.conv1 = GINConv(
            Sequential(Linear(input_dim, dim), BatchNorm1d(dim), ReLU(),
                       Linear(dim, dim), ReLU()))

        self.conv2 = GINConv(
            Sequential(Linear(dim, dim), BatchNorm1d(dim), ReLU(),
                       Linear(dim, dim), ReLU()))

        self.conv3 = GINConv(
            Sequential(Linear(dim, dim), BatchNorm1d(dim), ReLU(),
                       Linear(dim, dim), ReLU()))

        self.conv4 = GINConv(
            Sequential(Linear(dim, dim), BatchNorm1d(dim), ReLU(),
                       Linear(dim, dim), ReLU()))

        self.conv5 = GINConv(
            Sequential(Linear(dim, dim), BatchNorm1d(dim), ReLU(),
                       Linear(dim, dim), ReLU()))

        self.lin1 = Linear(dim, dim)
        self.lin2 = Linear(dim, num_classes)

    def forward(self, data, batch, edge_weights=None, get_embedding=False):
        x, edge_index = data.x, data.edge_index

        if edge_weights is None:
            edge_weights = torch.ones(edge_index.size(1))

        x = self.conv1(x, edge_index, edge_weights)
        x = self.conv2(x, edge_index, edge_weights)
        x = self.conv3(x, edge_index, edge_weights)
        x = self.conv4(x, edge_index, edge_weights)
        x = self.conv5(x, edge_index, edge_weights)
        if get_embedding:
            return x
        x = global_add_pool(x, batch)
        x = self.lin1(x).relu()
        x = F.dropout(x, p=0.5, training=self.training)
        x = self.lin2(x)
        return F.log_softmax(x, dim=-1)

class GraphGCN(torch.nn.Module):
    """
    A graph clasification model for graphs decribed in https://arxiv.org/abs/2011.04573.
    This model consists of 3 stacked GCN layers followed by a linear layer.
    In between the GCN outputs and linear layers are pooling operations in both mean and max.
    """
    def __init__(self, num_features, num_classes):
        super(GraphGCN, self).__init__()
        self.embedding_size = 20
        self.conv1 = GCNConv(num_features, 20)
        self.relu1 = ReLU()
        self.conv2 = GCNConv(20, 20)
        self.relu2 = ReLU()
        self.conv3 = GCNConv(20, 20)
        self.relu3 = ReLU()
        self.lin = Linear(self.embedding_size * 2, num_classes)

    def forward(self, data, batch=None, edge_weights=None):
        x, edge_index = data.x, data.edge_index
        if batch is None: # No batch given
            batch = torch.zeros(x.size(0), dtype=torch.long)
        embed = self.embedding(data, edge_weights)

        out1 = global_max_pool(embed, batch)
        out2 = global_mean_pool(embed, batch)
        input_lin = torch.cat([out1, out2], dim=-1)

        out = self.lin(input_lin)
        return F.log_softmax(out, dim=-1)

    def embedding(self, data, edge_weights=None):
        x, edge_index = data.x, data.edge_index
        if edge_weights is None:
            edge_weights = torch.ones(edge_index.size(1))
        
        #print(x.shape, edge_index.shape, edge_weights.shape)
        
        out1 = self.conv1(x, edge_index, edge_weights)
        out1 = torch.nn.functional.normalize(out1, p=2, dim=1)  # this is not used in PGExplainer
        out1 = self.relu1(out1)

        out2 = self.conv2(out1, edge_index, edge_weights)
        out2 = torch.nn.functional.normalize(out2, p=2, dim=1)  # this is not used in PGExplainer
        out2 = self.relu2(out2)

        out3 = self.conv3(out2, edge_index, edge_weights)
        out3 = torch.nn.functional.normalize(out3, p=2, dim=1)  # this is not used in PGExplainer
        out3 = self.relu3(out3)

        input_lin = out3

        return input_lin
